from django.shortcuts import render


def home(request):
    context = {}
    template = 'comingsoon/index.html'
    return render(request, template, context)

def login(request):
    context = {}
    template = 'comingsoon/login.html'
    return render(request, template, context)

def get_data(request):
    context = {}
    template = 'comingsoon/cards.html'
    return render(request, template, context)